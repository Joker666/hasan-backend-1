package repo

import (
	"context"

	"gorm.io/gorm"

	"gitlab.com/Joker666/hasan-backend-1/model"
)

// UserRepository interface represents the auth repository
type UserRepository interface {
	Add(ctx context.Context, v *model.User) error
	GetByEmail(ctx context.Context, email string) (*model.User, error)
}

// User holds the necessary fields for AuthRepository
type User struct {
	db *gorm.DB
}

// NewUser returns a new instance of Auth repository
func NewUser(db *gorm.DB) *User {
	return &User{
		db: db,
	}
}

// Add adds a new user to the repository
func (u *User) Add(ctx context.Context, v *model.User) error {
	tx := u.db.WithContext(ctx)
	result := tx.Create(&v)
	if result.Error != nil {
		return result.Error
	}
	return nil
}

// GetByEmail returns user by email
func (u *User) GetByEmail(ctx context.Context, email string) (*model.User, error) {
	tx := u.db.WithContext(ctx)
	bUsr := &model.User{}
	if err := tx.Where("email = ?", email).First(bUsr).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, nil
		}
		return nil, err
	}
	return bUsr, nil
}
