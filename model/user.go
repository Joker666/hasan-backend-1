package model

import (
	"time"

	"github.com/google/uuid"
)

// User model holds all user data
type User struct {
	ID        uuid.UUID `gorm:"type:uuid;default:uuid_generate_v4()"`
	Email     string    `gorm:"uniqueIndex"`
	Password  []byte
	CreatedAt time.Time
	UpdatedAt time.Time
}
