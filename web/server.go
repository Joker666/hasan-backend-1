package web

import (
	"net/http"

	"gitlab.com/Joker666/hasan-backend-1/web/controller"
	"gitlab.com/Joker666/hasan-backend-1/web/middleware"

	"github.com/gorilla/mux"
)

// NewServer adds routes to mux
func NewServer(router *mux.Router, userController *controller.UserController) {
	controller.AddCustomValidators()

	authRouter := router.PathPrefix("/auth").Subrouter()
	authRouter.HandleFunc("/register", userController.HandleRegister).Methods(http.MethodPost)
	authRouter.HandleFunc("/login", userController.HandleLogin).Methods(http.MethodPost)

	userRouter := router.PathPrefix("/user").Subrouter()
	auth := middleware.Auth(userController.S)
	userRouter.Use(auth)
	userRouter.HandleFunc("/me", userController.GetMe).Methods(http.MethodGet)
	userRouter.HandleFunc("/logout", userController.Logout).Methods(http.MethodGet)
}
