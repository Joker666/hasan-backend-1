package middleware

import (
	"context"
	"errors"
	"net/http"
	"strings"

	"github.com/gorilla/mux"

	"gitlab.com/Joker666/hasan-backend-1/service"
	"gitlab.com/Joker666/hasan-backend-1/web/resp"
)

// ContextKey hold the key of a context
type ContextKey string

// List of contexts
const (
	UserContext ContextKey = "user"
)

// GetUser returns user that's in context
func GetUser(r *http.Request) *service.Claims {
	v := r.Context().Value(UserContext)
	if v == nil {
		panic(errors.New("middleware: GetUser called without calling auth middleware prior"))
	}
	u, _ := v.(*service.Claims)
	return u
}

// Auth returns authentication middleware
func Auth(auth *service.UserService) mux.MiddlewareFunc {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			tok := r.Header.Get("Authorization")
			if !strings.HasPrefix(tok, "Bearer ") {
				resp.ServeUnauthorized(w, r, errors.New("unauthorized"))
				return
			}
			tok = strings.TrimSpace(strings.TrimPrefix(tok, "Bearer "))
			if tok == "" {
				resp.ServeUnauthorized(w, r, errors.New("unauthorized"))
				return
			}
			u, err := auth.GetUserClaimByToken(r.Context(), tok)
			if err != nil {
				resp.ServeUnauthorized(w, r, errors.New("unauthorized"))
				return
			}
			if u == nil {
				resp.ServeUnauthorized(w, r, errors.New("unauthorized"))
				return
			}
			ctx := context.WithValue(r.Context(), UserContext, u)
			next.ServeHTTP(w, r.WithContext(ctx))
		})
	}
}
