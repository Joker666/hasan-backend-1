package resp

// User holds necessary fields for a user
type User struct {
	ID              string `json:"ID"`
	Email           string `json:"Email"`
	UpdatedAt       string `json:"UpdatedAt"`
	CreatedAt       string `json:"CreatedAt"`
}

