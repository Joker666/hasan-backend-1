package controller

import (
	"net/http"

	"gitlab.com/Joker666/hasan-backend-1/web/middleware"

	"gitlab.com/Joker666/hasan-backend-1/model"

	"github.com/asaskevich/govalidator"
	"gitlab.com/Joker666/hasan-backend-1/service"
	"gitlab.com/Joker666/hasan-backend-1/web/resp"
)

// UserController holds necessary fields and data for serving auth handlers
type UserController struct {
	S *service.UserService
}

// NewUserController returns a new instance of UserController using Auth service
func NewUserController(s *service.UserService) *UserController {
	return &UserController{
		S: s,
	}
}

// RegisterBody holds the register data from request body
type RegisterBody struct {
	Email    string `json:"email"    valid:"required,email"`
	Password string `json:"password" valid:"required,length(10|20),hasuppercase,haslowercase,hasdigit,hasspecial"`
}

// HandleRegister handles user registration
func (u *UserController) HandleRegister(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	body := RegisterBody{}
	if err := ParseJSON(r.Body, &body); err != nil {
		resp.ServeBadRequest(w, r, err)
		return
	}

	if _, err := govalidator.ValidateStruct(&body); err != nil {
		dtls := ProcessValidationErr(err)
		resp.ServeUnprocessableEntity(w, r, ErrInvalidData, dtls)
		return
	}

	prevUsr, err := u.S.GetUserByEmail(ctx, body.Email)
	if err != nil {
		resp.ServeBadRequest(w, r, err)
		return
	}
	if prevUsr != nil {
		resp.ServeBadRequest(w, r, ErrUserAlreadyExists)
		return
	}

	usr, err := u.S.Register(ctx, body.Email, body.Password)
	if err != nil {
		resp.ServeBadRequest(w, r, err)
		return
	}

	tokenUser, err := u.S.Login(ctx, usr, body.Password)
	if err != nil {
		resp.ServeUnauthorized(w, r, ErrUnauthorized)
		return
	}

	resp.ServeData(w, r, http.StatusOK, tokenUser)
	return
}

// LoginBody holds the login data from request body
type LoginBody struct {
	Email    string `json:"email"    valid:"required,email"`
	Password string `json:"password" valid:"required"`
}

// HandleLogin handles user login
func (u *UserController) HandleLogin(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	body := LoginBody{}
	if err := ParseJSON(r.Body, &body); err != nil {
		resp.ServeBadRequest(w, r, err)
		return
	}

	if _, err := govalidator.ValidateStruct(&body); err != nil {
		dtls := ProcessValidationErr(err)
		resp.ServeUnprocessableEntity(w, r, ErrInvalidData, dtls)
		return
	}

	existingUsr, err := u.S.GetUserByEmail(ctx, body.Email)
	if err != nil {
		resp.ServeBadRequest(w, r, err)
		return
	}
	if existingUsr == nil {
		resp.ServeUnauthorized(w, r, ErrUnauthorized)
		return
	}

	tokenUser, err := u.S.Login(ctx, existingUsr, body.Password)
	if err != nil {
		resp.ServeUnauthorized(w, r, ErrUnauthorized)
		return
	}

	resp.ServeData(w, r, http.StatusOK, tokenUser)
	return
}

// GetMe serves login user
func (u *UserController) GetMe(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	usrClaim := middleware.GetUser(r)
	usr, err := u.S.GetUserByEmail(ctx, usrClaim.Email)
	if err != nil {
		resp.ServeUnauthorized(w, r, ErrUnauthorized)
		return
	}
	resp.ServeData(w, r, http.StatusOK, prepareUserResp(usr))
	return
}

// Logout logs user out of the system
func (u *UserController) Logout(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	usr := middleware.GetUser(r)
	u.S.RemoveUserTokenFromCache(ctx, usr.AccessUUID)
	resp.ServeData(w, r, http.StatusOK, nil)
	return
}

func prepareUserResp(u *model.User) *resp.User {
	usr := &resp.User{
		ID:        u.ID.String(),
		Email:     u.Email,
		CreatedAt: *resp.FormatTime(&u.CreatedAt),
		UpdatedAt: *resp.FormatTime(&u.UpdatedAt),
	}
	return usr
}
