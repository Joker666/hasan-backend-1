package controller

import "errors"

// list of errors
var (
	ErrInvalidData       = errors.New("invalid data")
	ErrUnauthorized      = errors.New("unauthorized")
	ErrUserNotFound      = errors.New("user not found")
	ErrForbidden         = errors.New("forbidden")
	ErrUserAlreadyExists = errors.New("user already exists")
)
