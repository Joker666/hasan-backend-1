package controller

import (
	"encoding/json"
	"io"
	"unicode"

	"github.com/asaskevich/govalidator"
)

// ProcessValidationErr processes validation error
func ProcessValidationErr(err error) map[string]interface{} {
	messages := govalidator.ErrorsByField(err)
	dtls := map[string]interface{}{}
	for k, v := range messages {
		dtls[k] = v
	}
	return dtls
}

// ParseJSON parses json
func ParseJSON(r io.Reader, v interface{}) error {
	return json.NewDecoder(r).Decode(v)
}

// AddCustomValidators adds custom validation rules to govalidator
func AddCustomValidators() {
	govalidator.CustomTypeTagMap.Set("hasuppercase", func(i interface{}, o interface{}) bool {
		input := i.(string)
		return govalidator.HasUpperCase(input)
	})

	govalidator.CustomTypeTagMap.Set("haslowercase", func(i interface{}, o interface{}) bool {
		input := i.(string)
		return govalidator.HasLowerCase(input)
	})

	govalidator.CustomTypeTagMap.Set("hasdigit", func(i interface{}, o interface{}) bool {
		input := i.(string)
		for _, r := range input {
			if unicode.IsDigit(r) {
				return true
			}
		}
		return false
	})

	govalidator.CustomTypeTagMap.Set("hasspecial", func(i interface{}, o interface{}) bool {
		input := i.(string)
		for _, r := range input {
			if govalidator.Contains("@#&%$*.", string(r)) {
				return true
			}
		}
		return false
	})
}
