## Simple Auth App

## Dependencies

There were couple of dependencies used to handle the task. Notably an ORM and mux for handling routes.

- [Gorilla Mux](https://github.com/gorilla/mux) for handling routes
- [Gorm](https://gorm.io/) for ORM
- [Cobra](https://github.com/spf13/cobra) for making CLI application
- [JWT-Go](github.com/dgrijalva/jwt-go) for handling JWT
- [Govalidator](https://github.com/asaskevich/govalidator) for validating requests and adding custom validations
- [Dotenv](github.com/joho/godotenv) for loading dot env files to the program
- [Go-Redis](github.com/go-redis/redis/v8) redis client for Golang
- [PQ](github.com/lib/pq) postgres driver for Golang
