package cmd

import (
	"context"
	"database/sql"
	"log"
	"net/http"
	"os"
	"strconv"
	"time"

	"github.com/go-redis/redis/v8"

	"gitlab.com/Joker666/hasan-backend-1/model"

	"gitlab.com/Joker666/hasan-backend-1/web/controller"

	"gitlab.com/Joker666/hasan-backend-1/repo"
	"gitlab.com/Joker666/hasan-backend-1/service"

	"gorm.io/driver/postgres"

	"gorm.io/gorm"

	"github.com/gorilla/mux"
	"github.com/joho/godotenv"

	// imports go official postgres driver
	_ "github.com/lib/pq"
	"github.com/spf13/cobra"
	"gitlab.com/Joker666/hasan-backend-1/web"
)

// srvCmd is the serve sub command to start the api server
var srvCmd = &cobra.Command{
	Use:   "serve",
	Short: "serve serves the api server",
	RunE:  serve,
}

func init() {
	//
}

func serve(cmd *cobra.Command, args []string) error {
	ctx := context.Background()
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}

	db, err := connectDB()
	if err != nil {
		return err
	}

	cache, err := connectCache(ctx)
	if err != nil {
		return err
	}

	userRepo := repo.NewUser(db)
	dur, _ := strconv.Atoi(os.Getenv("Duration"))
	userService := service.NewUserService(userRepo, cache, []byte(os.Getenv("JwtKey")),
		[]byte(os.Getenv("PassSalt")), time.Duration(dur))
	userController := controller.NewUserController(userService)

	port := ":" + os.Getenv("PORT")
	r := mux.NewRouter()
	s := r.PathPrefix("/api").Subrouter()
	web.NewServer(s, userController)
	var dir string
	r.PathPrefix("/").Handler(http.StripPrefix("/", http.FileServer(http.Dir(dir))))

	log.Println("Starting server on", port)
	srv := &http.Server{
		Handler:      r,
		Addr:         port,
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}

	return srv.ListenAndServe()
}

func connectCache(ctx context.Context) (*redis.Client, error) {
	opt, err := redis.ParseURL(os.Getenv("CACHE_URI"))
	if err != nil {
		return nil, err
	}

	rdb := redis.NewClient(opt)
	log.Println("Pinging Cache")
	v := rdb.Ping(ctx)
	log.Println(v)
	return rdb, nil
}

func connectDB() (*gorm.DB, error) {
	sqlDB, err := sql.Open("postgres", os.Getenv("DB_URI"))
	if err != nil {
		return nil, err
	}

	i := 0
	for {
		pingErr := sqlDB.Ping()
		log.Println("Pinging DB")
		if pingErr != nil {
			log.Println(pingErr)
			if i > 10 {
				log.Println("Could not connect to database, exiting")
				os.Exit(1)
			}
		} else {
			log.Println("Database Connected")
			break
		}
		time.Sleep(time.Duration(1) * time.Second)
		i = i + 1
	}

	db, err := gorm.Open(postgres.New(postgres.Config{
		Conn: sqlDB,
	}), &gorm.Config{})
	if err != nil {
		return nil, err
	}

	// Migrate the schema
	err = db.AutoMigrate(&model.User{})
	return db, err
}
