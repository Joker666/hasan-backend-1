package service

import "errors"

// ErrInvalidCred error is returned when user credential doesn't match
var ErrInvalidCred = errors.New("invalid credential")

// ErrTokenInvalid is returned when token provided is invalid
var ErrTokenInvalid = errors.New("invalid token")
