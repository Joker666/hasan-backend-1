package service

import (
	"bytes"
	"context"
	"crypto/sha256"
	"time"

	"github.com/google/uuid"

	"github.com/go-redis/redis/v8"

	"github.com/asaskevich/govalidator"
	"github.com/dgrijalva/jwt-go"
	"github.com/mitchellh/mapstructure"
	"gitlab.com/Joker666/hasan-backend-1/model"
	"gitlab.com/Joker666/hasan-backend-1/repo"
	"golang.org/x/crypto/pbkdf2"
)

// Claims contains claims
type Claims struct {
	AccessUUID string
	UserID     string
	Email      string
	*jwt.StandardClaims
}

// UserService is the authentication service
type UserService struct {
	r     repo.UserRepository
	cache *redis.Client

	jwtKey   []byte
	passSalt []byte
	duration time.Duration
}

// NewUserService returns a new instance of auth service
func NewUserService(r repo.UserRepository, c *redis.Client, jwtKey, passSalt []byte, duration time.Duration) *UserService {
	return &UserService{
		r:        r,
		cache:    c,
		jwtKey:   jwtKey,
		passSalt: passSalt,
		duration: duration,
	}
}

// TokenInfo is a structure token
type TokenInfo struct {
	UserID     string
	Email      string
	Token      string
	LoggedInAt time.Time
	ExpiresAt  time.Time
}

// Register registers a user
func (u *UserService) Register(ctx context.Context, email, password string) (*model.User, error) {
	t := time.Now()
	usr := &model.User{
		Email:     email,
		CreatedAt: t,
		UpdatedAt: t,
	}
	usr.Email, _ = govalidator.NormalizeEmail(usr.Email)
	pass, err := u.hashPassword(password)
	if err != nil {
		return nil, err
	}
	usr.Password = pass

	if err := u.r.Add(ctx, usr); err != nil {
		return nil, err
	}

	return usr, nil
}

// Login logs in a user
func (u *UserService) Login(ctx context.Context, user *model.User, password string) (*TokenInfo, error) {
	ok, err := u.verifyPassword(user.Password, password)
	if err != nil {
		return nil, err
	} else if !ok {
		return nil, ErrInvalidCred
	}

	data, err := u.getTokenUser(ctx, user)
	if err != nil {
		return nil, err
	}
	return data, nil
}

// GetUserByEmail returns user by email
func (u *UserService) GetUserByEmail(ctx context.Context, email string) (*model.User, error) {
	usr, err := u.r.GetByEmail(ctx, email)
	if err != nil {
		return nil, err
	}
	return usr, nil
}

// GetUserClaimByToken returns user by token
func (u *UserService) GetUserClaimByToken(ctx context.Context, tok string) (*Claims, error) {
	token, err := jwt.Parse(tok, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, ErrTokenInvalid
		}
		return u.jwtKey, nil
	})
	if err != nil {
		return nil, err
	} else if !token.Valid {
		return nil, ErrTokenInvalid
	}

	usr := Claims{}
	if err := mapstructure.Decode(token.Claims, &usr); err != nil {
		return nil, err
	}

	_, err = u.cache.Get(ctx, usr.AccessUUID).Result()
	if err == redis.Nil {
		return nil, ErrTokenInvalid
	}

	return &usr, nil
}

// RemoveUserTokenFromCache removes token from cache
func (u *UserService) RemoveUserTokenFromCache(ctx context.Context, accessID string) {
	_, _ = u.cache.Del(ctx, accessID).Result()
}

func (u *UserService) getTokenUser(ctx context.Context, usr *model.User) (*TokenInfo, error) {
	now := time.Now()
	expirationTime := now.Add(u.duration * time.Minute)
	claims := &Claims{
		AccessUUID: uuid.New().String(),
		UserID:     usr.ID.String(),
		Email:      usr.Email,
		StandardClaims: &jwt.StandardClaims{
			ExpiresAt: expirationTime.Unix(),
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	tokenString, err := token.SignedString(u.jwtKey)
	if err != nil {
		return nil, err
	}

	data := &TokenInfo{
		Token:      tokenString,
		UserID:     usr.ID.String(),
		Email:      usr.Email,
		LoggedInAt: now,
		ExpiresAt:  expirationTime,
	}

	u.cache.Set(ctx, claims.AccessUUID, claims.UserID, u.duration*time.Minute)

	return data, nil
}

func (u *UserService) hashPassword(pass string) ([]byte, error) {
	passIter := 10000

	passHash := pbkdf2.Key([]byte(pass), u.passSalt, passIter, 32, sha256.New)
	return passHash, nil
}

func (u *UserService) verifyPassword(usrPass []byte, pass string) (bool, error) {
	cPass, err := u.hashPassword(pass)
	if err != nil {
		return false, err
	}
	return bytes.Equal(usrPass, cPass), nil
}
